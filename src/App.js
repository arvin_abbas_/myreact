import React, { Component } from "react";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import "./App.css";
import ReactDOM from "react-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.ClearText = this.ClearText.bind(this);
    this.state = {
      username: "salam"
    };
  }
  componentWillMount() {
    console.log("componentWillMount");
  }
  componentDidMount() {
    console.log("componentDidMount");
  }
  componentWillReceiveProps() {
    console.log("componentWillReceiveProps");
  }
  shouldComponentUpdate(newProps, newState) {
    console.log("shouldComponentUpdate");
    return true;
  }
  componentWillUpdate(nextProps, nextState) {
    console.log("componentWillUpdate");
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
  }
  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  ClearText() {
    this.setState({
      username: ""
    });
    ReactDOM.findDOMNode(this.refs.myInput).focus();
  }

  render() {
    return (
      <div className="App">
        <Header title="صفحه ی اصلی" className="Header" />
        <button onClick={this.ClearText}>Clear text</button>
        <input
          type="text"
          placeholder="username"
          value={this.state.username}
          onChange={e => this.setState({ username: e.target.value })}
          ref="myInput"
        />
        <h1>{this.state.username}</h1>
        <Footer className="Footer" />
      </div>
    );
  }
}

export default App;
