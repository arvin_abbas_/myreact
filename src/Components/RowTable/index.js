import React, { Component } from "react";

export class index extends Component {
  render() {
    return (
      <tr>
        <td>{this.props.data.name}</td>
        <td>{this.props.data.family}</td>
        <td>{this.props.data.age}</td>
      </tr>
    );
  }
}

export default index;
